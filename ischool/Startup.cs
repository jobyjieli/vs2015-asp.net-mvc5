﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ischool.Startup))]
namespace ischool
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
