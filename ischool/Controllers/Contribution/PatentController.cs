﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ischool.Models;
using Microsoft.AspNet.Identity;

namespace ischool.Controllers
{
    [Authorize]
    public class PatentController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private string ViewDirectory(string nav)
        {
            string s = "~/Views/Contribution/Patent/" + nav + ".cshtml";
            return s;
        }
        // GET: Patent
        public async Task<ActionResult> Index()
        {
            var userId = User.Identity.GetUserId();
            var patent = db.Patent.Where(p => p.StaffInfoId == userId);
            return View(ViewDirectory("Index"), await patent.ToListAsync());
        }

        // GET: Patent/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            Patent patent = await db.Patent.FindAsync(id);
            if (patent == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (patent.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限查看该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            return View(ViewDirectory("Details"), patent);
        }

        // GET: Patent/Create
        public ActionResult Create()
        {
            var userId = User.Identity.GetUserId();
            ViewBag.StaffInfoId = userId;
            return View(ViewDirectory("Create"));
        }

        // POST: Patent/Create
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,StaffInfoId,Name,Time,Type,Score,TotalNum,Sequence,PersonalScore")] Patent patent)
        {
            var userId = User.Identity.GetUserId();
            var patentType = patent.Type;
            ViewBag.StaffInfoId = userId;
            switch (patentType)
            {
                case PatentType.国家发明专利:
                    patent.Score = 12; break;
                case PatentType.软件著作权:
                    patent.Score = 5; break;
                default:
                    patent.Score = 3; break;
            }
            if (patent.TotalNum <= 0)
            {
                ModelState.AddModelError("TotalNum", "完成人员总数必须大于零");
            }
            if (patent.Sequence <= 0)
            {
                ModelState.AddModelError("Sequence", "个人排名必须大于零");
            }
            else if (patent.Sequence > patent.TotalNum)
            {
                ModelState.AddModelError("Sequence", "个人排名不得大于完成人员总数");
            }

            if (ModelState.IsValid)
            {
                db.Patent.Add(patent);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(ViewDirectory("Create"), patent);
        }

        // GET: Patent/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            Patent patent = await db.Patent.FindAsync(id);
            if (patent == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (patent.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限查看该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            ViewBag.StaffInfoId = userId;
            return View(ViewDirectory("Edit"), patent);
        }

        // POST: Patent/Edit/5
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,StaffInfoId,Name,Time,Type,Score,TotalNum,Sequence,PersonalScore")] Patent patent)
        {
            var userId = User.Identity.GetUserId();
            var patentType = patent.Type;
            ViewBag.StaffInfoId = userId;
            switch (patentType)
            {
                case PatentType.国家发明专利:
                    patent.Score = 12; break;
                case PatentType.软件著作权:
                    patent.Score = 5; break;
                default:
                    patent.Score = 3; break;
            }
            if (patent.TotalNum <= 0)
            {
                ModelState.AddModelError("TotalNum", "完成人员总数必须大于零");
            }
            if (patent.Sequence <= 0)
            {
                ModelState.AddModelError("Sequence", "个人排名必须大于零");
            }
            else if (patent.Sequence > patent.TotalNum)
            {
                ModelState.AddModelError("Sequence", "个人排名不得大于完成人员总数");
            }

            if (ModelState.IsValid)
            {
                db.Entry(patent).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(ViewDirectory("Edit"), patent);
        }

        // GET: Patent/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            Patent patent = await db.Patent.FindAsync(id);
            if (patent == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (patent.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限查看该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            return View(ViewDirectory("Delete"), patent);
        }

        // POST: Patent/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Patent patent = await db.Patent.FindAsync(id);
            db.Patent.Remove(patent);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
