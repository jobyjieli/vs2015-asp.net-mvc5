﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ischool.Models;
using Microsoft.AspNet.Identity;

namespace ischool.Controllers
{
    [Authorize]
    public class TeacherSkillController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private string ViewDirectory(string nav)
        {
            string s = "~/Views/Contribution/TeacherSkill/" + nav + ".cshtml";
            return s;
        }
        int[,] alTable = new int[3, 5] { { 20,16,14,12,0 }, { 16, 12, 10, 10, 0 }, { 12, 10, 8, 5, 2 } };

        // GET: TeacherSkill
        public async Task<ActionResult> Index()
        {
            var userId = User.Identity.GetUserId();
            var teacherSkill = db.TeacherSkill.Where(t => t.StaffInfoId == userId);
            return View(ViewDirectory("Index"), await teacherSkill.ToListAsync());
        }

        // GET: TeacherSkill/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            TeacherSkill teacherSkill = await db.TeacherSkill.FindAsync(id);
            if (teacherSkill == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (teacherSkill.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限查看该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            return View(ViewDirectory("Details"), teacherSkill);
        }

        // GET: TeacherSkill/Create
        public ActionResult Create()
        {
            var userId = User.Identity.GetUserId();
            ViewBag.StaffInfoId = userId;
            return View(ViewDirectory("Create"));
        }

        // POST: TeacherSkill/Create
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,StaffInfoId,Content,Time,Level,Award,Score")] TeacherSkill teacherSkill)
        {
            var userId = User.Identity.GetUserId();
            var teacherSkilllevel = teacherSkill.Level;
            ViewBag.StaffInfoId = userId;
            if (teacherSkill.Level != Level2.校级 & teacherSkill.Award == Prize.校级申报未取 )
            {
                ModelState.AddModelError("Award", "申报未果仅限校级");
            }
            teacherSkill.Score = alTable[(int)teacherSkill.Level, (int)teacherSkill.Award];

            if (ModelState.IsValid)
            {
                db.TeacherSkill.Add(teacherSkill);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(ViewDirectory("Create"), teacherSkill);
        }

        // GET: TeacherSkill/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            TeacherSkill teacherSkill = await db.TeacherSkill.FindAsync(id);
            if (teacherSkill == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (teacherSkill.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限查看该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            ViewBag.StaffInfoId = userId;
            return View(ViewDirectory("Edit"), teacherSkill);
        }

        // POST: TeacherSkill/Edit/5
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,StaffInfoId,Content,Time,Level,Award,Score")] TeacherSkill teacherSkill)
        {
            var userId = User.Identity.GetUserId();
            var teacherSkilllevel = teacherSkill.Level;
            ViewBag.StaffInfoId = userId;
            if (teacherSkill.Level != Level2.校级 & teacherSkill.Award == Prize.校级申报未取)
            {
                ModelState.AddModelError("Award", "申报未果仅限校级");
            }
            teacherSkill.Score = alTable[(int)teacherSkill.Level, (int)teacherSkill.Award];

            if (ModelState.IsValid)
            {
                db.Entry(teacherSkill).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(ViewDirectory("Edit"), teacherSkill);
        }

        // GET: TeacherSkill/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            TeacherSkill teacherSkill = await db.TeacherSkill.FindAsync(id);
            if (teacherSkill == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (teacherSkill.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限查看该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            return View(ViewDirectory("Delete"), teacherSkill);
        }

        // POST: TeacherSkill/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            TeacherSkill teacherSkill = await db.TeacherSkill.FindAsync(id);
            db.TeacherSkill.Remove(teacherSkill);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
