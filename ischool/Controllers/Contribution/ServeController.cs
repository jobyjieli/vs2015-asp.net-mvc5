﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ischool.Models;
using Microsoft.AspNet.Identity;

namespace ischool.Controllers
{
    [Authorize]
    public class ServeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private string ViewDirectory(string nav)
        {
            string s = "~/Views/Contribution/Serve/" + nav + ".cshtml";
            return s;
        }
        // GET: Serve
        public async Task<ActionResult> Index()
        {
            var userId = User.Identity.GetUserId();
            var serve = db.Serve.Where(s => s.StaffInfoId == userId);
            return View(ViewDirectory("Index"), await serve.ToListAsync());
        }

        // GET: Serve/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            Serve serve = await db.Serve.FindAsync(id);
            if (serve == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (serve.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限查看该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            return View(ViewDirectory("Details"), serve);
        }

        // GET: Serve/Create
        public ActionResult Create()
        {
            var userId = User.Identity.GetUserId();
            ViewBag.StaffInfoId = userId;
            return View(ViewDirectory("Create"));
        }

        // POST: Serve/Create
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,StaffInfoId,Type,Content,Time,Amount,Score,TotalNum,Sequence,PersonalScore")] Serve serve)
        {
            var userId = User.Identity.GetUserId();
            var serveType = serve.Type;
            ViewBag.StaffInfoId = userId;
            if (serve.Amount <= 0)
            {
                ModelState.AddModelError("Amount", "金额必须大于零");
            }
            switch (serveType)
            {
                case ServeType.创收服务或中心可支配留成经费:
                    serve.Score = serve.Amount*10; break;
                case ServeType.校外企业赞助学生竞赛:
                    serve.Score = serve.Amount*2; break;
                default:
                    serve.Score = serve.Amount; break;
            }
            if (serve.TotalNum <= 0)
            {
                ModelState.AddModelError("TotalNum", "完成人员总数必须大于零");
            }
            if (serve.Sequence <= 0)
            {
                ModelState.AddModelError("Sequence", "个人排名必须大于零");
            }
            else if (serve.Sequence > serve.TotalNum)
            {
                ModelState.AddModelError("Sequence", "个人排名不得大于完成人员总数");
            }

            if (ModelState.IsValid)
            {
                db.Serve.Add(serve);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(ViewDirectory("Create"), serve);
        }

        // GET: Serve/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            Serve serve = await db.Serve.FindAsync(id);
            if (serve == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (serve.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限查看该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            ViewBag.StaffInfoId = userId;
            return View(ViewDirectory("Edit"), serve);
        }

        // POST: Serve/Edit/5
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,StaffInfoId,Type,Content,Time,Amount,Score,TotalNum,Sequence,PersonalScore")] Serve serve)
        {
            var userId = User.Identity.GetUserId();
            var serveType = serve.Type;
            ViewBag.StaffInfoId = userId;
            if (serve.Amount <= 0)
            {
                ModelState.AddModelError("Amount", "金额必须大于零");
            }
            switch (serveType)
            {
                case ServeType.创收服务或中心可支配留成经费:
                    serve.Score = serve.Amount * 10; break;
                case ServeType.校外企业赞助学生竞赛:
                    serve.Score = serve.Amount * 2; break;
                default:
                    serve.Score = serve.Amount; break;
            }
            if (serve.TotalNum <= 0)
            {
                ModelState.AddModelError("TotalNum", "完成人员总数必须大于零");
            }
            if (serve.Sequence <= 0)
            {
                ModelState.AddModelError("Sequence", "个人排名必须大于零");
            }
            else if (serve.Sequence > serve.TotalNum)
            {
                ModelState.AddModelError("Sequence", "个人排名不得大于完成人员总数");
            }

            if (ModelState.IsValid)
            {
                db.Entry(serve).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(ViewDirectory("Edit"), serve);
        }

        // GET: Serve/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            Serve serve = await db.Serve.FindAsync(id);
            if (serve == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (serve.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限查看该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            return View(ViewDirectory("Delete"), serve);
        }

        // POST: Serve/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Serve serve = await db.Serve.FindAsync(id);
            db.Serve.Remove(serve);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
