﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ischool.Models;
using Microsoft.AspNet.Identity;

namespace ischool.Controllers
{
    [Authorize]
    public class ResearchController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private string ViewDirectory(string nav)
        {
            string s = "~/Views/Contribution/Research/" + nav + ".cshtml";
            return s;
        }
        // GET: Research
        public async Task<ActionResult> Index()
        {
            var userId = User.Identity.GetUserId();
            var research = db.Research.Where(r => r.StaffInfoId == userId);
            return View(ViewDirectory("Index"), await research.ToListAsync());
        }

        // GET: Research/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            Research research = await db.Research.FindAsync(id);
            if (research == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (research.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限查看该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            return View(ViewDirectory("Details"), research);
        }

        // GET: Research/Create
        public ActionResult Create()
        {
            var userId = User.Identity.GetUserId();
            ViewBag.StaffInfoId = userId;
            return View(ViewDirectory("Create"));
        }

        // POST: Research/Create
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,StaffInfoId,Content,Time,Level,Score,TotalNum,Sequence,PersonalScore")] Research research)
        {
            var userId = User.Identity.GetUserId();
            var researchlevel = research.Level;
            ViewBag.StaffInfoId = userId;
            switch (researchlevel)
            {
                case ResearchLevel.国家级教改及科研:
                    research.Score = 30; break;
                case ResearchLevel.省级教改及科研:
                    research.Score = 15; break;
                case ResearchLevel.校级教改:
                    research.Score = 10; break;
                case ResearchLevel.院级教改:
                    research.Score = 6; break;
                default:
                    research.Score = 2; break;
            }
            if (research.TotalNum <= 0)
            {
                ModelState.AddModelError("TotalNum", "完成人员总数必须大于零");
            }
            if (research.Sequence <= 0)
            {
                ModelState.AddModelError("Sequence", "个人排名必须大于零");
            }
            else if (research.Sequence > research.TotalNum)
            {
                ModelState.AddModelError("Sequence", "个人排名不得大于完成人员总数");
            }

            if (ModelState.IsValid)
            {
                db.Research.Add(research);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(ViewDirectory("Create"), research);
        }

        // GET: Research/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            Research research = await db.Research.FindAsync(id);
            if (research == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (research.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限查看该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            ViewBag.StaffInfoId = userId;
            return View(ViewDirectory("Edit"), research);
        }

        // POST: Research/Edit/5
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,StaffInfoId,Content,Time,Level,Score,TotalNum,Sequence,PersonalScore")] Research research)
        {
            var userId = User.Identity.GetUserId();
            var researchlevel = research.Level;
            ViewBag.StaffInfoId = userId;
            switch (researchlevel)
            {
                case ResearchLevel.国家级教改及科研:
                    research.Score = 30; break;
                case ResearchLevel.省级教改及科研:
                    research.Score = 15; break;
                case ResearchLevel.校级教改:
                    research.Score = 10; break;
                case ResearchLevel.院级教改:
                    research.Score = 6; break;
                default:
                    research.Score = 2; break;
            }
            if (research.TotalNum <= 0)
            {
                ModelState.AddModelError("TotalNum", "完成人员总数必须大于零");
            }
            if (research.Sequence <= 0)
            {
                ModelState.AddModelError("Sequence", "个人排名必须大于零");
            }
            else if (research.Sequence > research.TotalNum)
            {
                ModelState.AddModelError("Sequence", "个人排名不得大于完成人员总数");
            }

            if (ModelState.IsValid)
            {
                db.Entry(research).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(ViewDirectory("Edit"), research);
        }

        // GET: Research/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            Research research = await db.Research.FindAsync(id);
            if (research == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (research.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限查看该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            return View(ViewDirectory("Delete"), research);
        }

        // POST: Research/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Research research = await db.Research.FindAsync(id);
            db.Research.Remove(research);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
