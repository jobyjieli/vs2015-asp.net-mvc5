﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ischool.Models;
using Microsoft.AspNet.Identity;

namespace ischool.Controllers
{
    [Authorize]
    public class PaperController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private string ViewDirectory(string nav)
        {
            string s = "~/Views/Contribution/Paper/" + nav + ".cshtml";
            return s;
        }
        // GET: Paper
        public async Task<ActionResult> Index()
        {
            var userId = User.Identity.GetUserId();
            var paper = db.Paper.Where(t => t.StaffInfoId == userId);
            return View(ViewDirectory("Index"), await paper.ToListAsync());
        }

        // GET: Paper/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            Paper paper = await db.Paper.FindAsync(id);
            if (paper == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (paper.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限查看该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            return View(ViewDirectory("Details"), paper);
        }

        // GET: Paper/Create
        public ActionResult Create()
        {
            var userId = User.Identity.GetUserId();
            ViewBag.StaffInfoId = userId;
            return View(ViewDirectory("Create"));
        }

        // POST: Paper/Create
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,StaffInfoId,Title,Journal,Time,Type,Score,TotalNum,Sequence,PersonalScore")] Paper paper)
        {
            var userId = User.Identity.GetUserId();
            var papertype = paper.Type;
            ViewBag.StaffInfoId = userId;

            switch (papertype)
            {
                case PaperType.SCI刊物:
                    paper.Score = 20; break;
                case PaperType.EI刊物:
                    paper.Score = 10; break;
                case PaperType.核心期刊:
                    paper.Score = 6; break;
                default:
                    paper.Score = 3; break;
            }
            if (paper.TotalNum <= 0)
            {
                ModelState.AddModelError("TotalNum", "完成人员总数必须大于零");
            }
            if (paper.Sequence <= 0)
            {
                ModelState.AddModelError("Sequence", "个人排名必须大于零");
            }
            else if (paper.Sequence > paper.TotalNum)
            {
                ModelState.AddModelError("Sequence", "个人排名不得大于完成人员总数");
            }

            if (ModelState.IsValid)
            {
                db.Paper.Add(paper);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(ViewDirectory("Create"), paper);
        }

        // GET: Paper/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            Paper paper = await db.Paper.FindAsync(id);
            if (paper == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (paper.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限查看该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            ViewBag.StaffInfoId = userId;
            return View(ViewDirectory("Edit"), paper);
        }

        // POST: Paper/Edit/5
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,StaffInfoId,Title,Journal,Time,Type,Score,TotalNum,Sequence,PersonalScore")] Paper paper)
        {
            var userId = User.Identity.GetUserId();
            var papertype = paper.Type;
            ViewBag.StaffInfoId = userId;

            switch (papertype)
            {
                case PaperType.SCI刊物:
                    paper.Score = 20; break;
                case PaperType.EI刊物:
                    paper.Score = 10; break;
                case PaperType.核心期刊:
                    paper.Score = 6; break;
                default:
                    paper.Score = 3; break;
            }
            if (paper.TotalNum <= 0)
            {
                ModelState.AddModelError("TotalNum", "完成人员总数必须大于零");
            }
            if (paper.Sequence <= 0)
            {
                ModelState.AddModelError("Sequence", "个人排名必须大于零");
            }
            else if (paper.Sequence > paper.TotalNum)
            {
                ModelState.AddModelError("Sequence", "个人排名不得大于完成人员总数");
            }

            if (ModelState.IsValid)
            {
                db.Entry(paper).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(ViewDirectory("Edit"), paper);
        }

        // GET: Paper/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            Paper paper = await db.Paper.FindAsync(id);
            if (paper == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (paper.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限查看该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            return View(ViewDirectory("Delete"), paper);
        }

        // POST: Paper/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Paper paper = await db.Paper.FindAsync(id);
            db.Paper.Remove(paper);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
