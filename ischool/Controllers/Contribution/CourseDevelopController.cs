﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ischool.Models;
using Microsoft.AspNet.Identity;

namespace ischool.Controllers
{
    [Authorize]
    public class CourseDevelopController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private string ViewDirectory(string nav)
        {
            string s = "~/Views/Contribution/CourseDevelop/" + nav + ".cshtml";
            return s;
        }
        // GET: CourseDevelop
        public async Task<ActionResult> Index()
        {
            var userId = User.Identity.GetUserId();
            var courseDevelop = db.CourseDevelop.Where(a => a.StaffInfoId == userId);
            return View(ViewDirectory("Index"), await courseDevelop.ToListAsync());
        }

        // GET: CourseDevelop/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            CourseDevelop courseDevelop = await db.CourseDevelop.FindAsync(id);
            if (courseDevelop == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (courseDevelop.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限查看该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            return View(ViewDirectory("Details"), courseDevelop);
        }

        // GET: CourseDevelop/Create
        public ActionResult Create()
        {
            var userId = User.Identity.GetUserId();
            ViewBag.StaffInfoId = userId;
            return View(ViewDirectory("Create"));
        }

        // POST: CourseDevelop/Create
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,StaffInfoId,Content,Time,Level,Score,TotalNum,Sequence,PersonalScore")] CourseDevelop courseDevelop)
        {
            var userId = User.Identity.GetUserId();
            var courseDeveloplevel = courseDevelop.Level;
            ViewBag.StaffInfoId = userId;

            switch (courseDeveloplevel)
            {
                case Level.国家级:
                    courseDevelop.Score = 40; break;
                case Level.省级:
                    courseDevelop.Score = 20; break;
                case Level.校级:
                    courseDevelop.Score = 12; break;
                default:
                    courseDevelop.Score = 3; break;
            }
            if (courseDevelop.TotalNum <= 0)
            {
                ModelState.AddModelError("TotalNum", "完成人员总数必须大于零");
            }
            if (courseDevelop.Sequence <= 0)
            {
                ModelState.AddModelError("Sequence", "个人排名必须大于零");
            }
            else if (courseDevelop.Sequence > courseDevelop.TotalNum)
            {
                ModelState.AddModelError("Sequence", "个人排名不得大于完成人员总数");
            }

            if (ModelState.IsValid)
            {
                db.CourseDevelop.Add(courseDevelop);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(ViewDirectory("Create"), courseDevelop);
        }

        // GET: CourseDevelop/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            CourseDevelop courseDevelop = await db.CourseDevelop.FindAsync(id);
            if (courseDevelop == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (courseDevelop.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限编辑该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            ViewBag.StaffInfoId = userId;
            return View(ViewDirectory("Edit"), courseDevelop);
        }

        // POST: CourseDevelop/Edit/5
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,StaffInfoId,Content,Time,Level,Score,TotalNum,Sequence,PersonalScore")] CourseDevelop courseDevelop)
        {
            var userId = User.Identity.GetUserId();
            var courseDeveloplevel = courseDevelop.Level;
            ViewBag.StaffInfoId = userId;

            switch (courseDeveloplevel)
            {
                case Level.国家级:
                    courseDevelop.Score = 40; break;
                case Level.省级:
                    courseDevelop.Score = 20; break;
                case Level.校级:
                    courseDevelop.Score = 12; break;
                default:
                    courseDevelop.Score = 3; break;
            }
            if (courseDevelop.TotalNum <= 0)
            {
                ModelState.AddModelError("TotalNum", "完成人员总数必须大于零");
            }
            if (courseDevelop.Sequence <= 0)
            {
                ModelState.AddModelError("Sequence", "个人排名必须大于零");
            }
            else if (courseDevelop.Sequence > courseDevelop.TotalNum)
            {
                ModelState.AddModelError("Sequence", "个人排名不得大于完成人员总数");
            }

            if (ModelState.IsValid)
            {
                db.Entry(courseDevelop).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(ViewDirectory("Edit"), courseDevelop);
        }

        // GET: CourseDevelop/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            CourseDevelop courseDevelop = await db.CourseDevelop.FindAsync(id);
            if (courseDevelop == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (courseDevelop.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限删除该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            return View(ViewDirectory("Delete"), courseDevelop);
        }

        // POST: CourseDevelop/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            CourseDevelop courseDevelop = await db.CourseDevelop.FindAsync(id);
            db.CourseDevelop.Remove(courseDevelop);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
