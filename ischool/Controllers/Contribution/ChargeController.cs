﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ischool.Models;
using Microsoft.AspNet.Identity;

namespace ischool.Controllers
{
    [Authorize]
    public class ChargeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private string ViewDirectory(string nav)
        {
            string s = "~/Views/Contribution/Charge/" + nav + ".cshtml";
            return s;
        }
        //scoreC:全体贡献原始分取平均
        public float scoreC = 0;

        // GET: Charge
        public async Task<ActionResult> Index()
        {
            var userId = User.Identity.GetUserId();
            var charge = db.Charge.Where(c => c.StaffInfoId == userId);
            return View(ViewDirectory("Index"),await charge.ToListAsync());
        }

        // GET: Charge/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            Charge charge = await db.Charge.FindAsync(id);
            if (charge == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (charge.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限查看该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            return View(ViewDirectory("Details"),charge);
        }

        // GET: Charge/Create
        public ActionResult Create()
        {
            var userId = User.Identity.GetUserId();
            ViewBag.StaffInfoId = userId;
            return View(ViewDirectory("Create"));
        }

        // POST: Charge/Create
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,StaffInfoId,Title,Duty,Score")] Charge charge)
        {
            var userId = User.Identity.GetUserId();
            var chargeDuty = charge.Duty;
            ViewBag.StaffInfoId = userId;

            switch (chargeDuty)
            {
                case DutyType.系中心副主任或书记:
                    charge.Score = scoreC / 3; break;
                case DutyType.平台主任:
                    charge.Score = scoreC / 4; break;
                case DutyType.平台副主任:
                    charge.Score = scoreC / 5; break;
                case DutyType.校外重要学术团体兼任职务:
                    charge.Score = scoreC / 4; break;
                default:
                    charge.Score = scoreC / 5; break;
            }

            if (ModelState.IsValid)
            {
                db.Charge.Add(charge);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(ViewDirectory("Create"),charge);
        }

        // GET: Charge/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            Charge charge = await db.Charge.FindAsync(id);
            if (charge == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (charge.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限查看该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            ViewBag.StaffInfoId = userId;
            return View(ViewDirectory("Edit"),charge);
        }

        // POST: Charge/Edit/5
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,StaffInfoId,Title,Duty,Score")] Charge charge)
        {
            var userId = User.Identity.GetUserId();
            var chargeDuty = charge.Duty;
            ViewBag.StaffInfoId = userId;

            switch (chargeDuty)
            {
                case DutyType.系中心副主任或书记:
                    charge.Score = scoreC / 3; break;
                case DutyType.平台主任:
                    charge.Score = scoreC / 4; break;
                case DutyType.平台副主任:
                    charge.Score = scoreC / 5; break;
                case DutyType.校外重要学术团体兼任职务:
                    charge.Score = scoreC / 4; break;
                default:
                    charge.Score = scoreC / 5; break;
            }

            if (ModelState.IsValid)
            {
                db.Entry(charge).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(ViewDirectory("Edit"),charge);
        }

        // GET: Charge/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            Charge charge = await db.Charge.FindAsync(id);
            if (charge == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (charge.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限查看该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            return View(ViewDirectory("Delete"),charge);
        }

        // POST: Charge/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Charge charge = await db.Charge.FindAsync(id);
            db.Charge.Remove(charge);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
