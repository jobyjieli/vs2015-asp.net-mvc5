﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ischool.Models;
using Microsoft.AspNet.Identity;

namespace ischool.Controllers
{
    [Authorize]
    public class SafeAndProduceController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private string ViewDirectory(string nav)
        {
            string s = "~/Views/Contribution/SafeAndProduce/" + nav + ".cshtml";
            return s;
        }
        // GET: SafeAndProduce
        public async Task<ActionResult> Index()
        {
            var userId = User.Identity.GetUserId();
            var safeAndProduce = db.SafeAndProduce.Where(s => s.StaffInfoId == userId);
            return View(ViewDirectory("Index"), await safeAndProduce.ToListAsync());
        }

        // GET: SafeAndProduce/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            SafeAndProduce safeAndProduce = await db.SafeAndProduce.FindAsync(id);
            if (safeAndProduce == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (safeAndProduce.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限查看该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            return View(ViewDirectory("Details"), safeAndProduce);
        }

        // GET: SafeAndProduce/Create
        public ActionResult Create()
        {
            var userId = User.Identity.GetUserId();
            ViewBag.StaffInfoId = userId;
            return View(ViewDirectory("Create"));
        }

        // POST: SafeAndProduce/Create
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,StaffInfoId,Type,Room,Amount,Time,Score,PersonalScore")] SafeAndProduce safeAndProduce)
        {
            var userId = User.Identity.GetUserId();
            var safeAndProduceType = safeAndProduce.Type;
            ViewBag.StaffInfoId = userId;
            switch (safeAndProduceType)
            {
                case SafeAndProduceType.实验室安全卫生良好:
                    if(safeAndProduce.Room == null)
                    {
                        ModelState.AddModelError("Room", "选择实验室安全卫生良好房间号为必填");
                    }
                    safeAndProduce.Score = 6;
                    break;
                default:
                    if (safeAndProduce.Amount.HasValue == false)
                    {
                        ModelState.AddModelError("Amount", "选择自制实验设备金额为必填"); break;
                    }
                    else if(safeAndProduce.Amount.Value <= 0)
                    {
                        ModelState.AddModelError("Amount", "选择自制实验设备金额为必填"); break;
                    }
                    safeAndProduce.Score = safeAndProduce.Amount.Value*3;
                    break;
            }

            if (ModelState.IsValid)
            {
                db.SafeAndProduce.Add(safeAndProduce);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(ViewDirectory("Create"), safeAndProduce);
        }

        // GET: SafeAndProduce/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            SafeAndProduce safeAndProduce = await db.SafeAndProduce.FindAsync(id);
            if (safeAndProduce == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (safeAndProduce.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限查看该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            ViewBag.StaffInfoId = userId;
            return View(ViewDirectory("Edit"), safeAndProduce);
        }

        // POST: SafeAndProduce/Edit/5
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,StaffInfoId,Type,Room,Amount,Time,Score,PersonalScore")] SafeAndProduce safeAndProduce)
        {
            var userId = User.Identity.GetUserId();
            var safeAndProduceType = safeAndProduce.Type;
            ViewBag.StaffInfoId = userId;
            switch (safeAndProduceType)
            {
                case SafeAndProduceType.实验室安全卫生良好:
                    if (safeAndProduce.Room == null)
                    {
                        ModelState.AddModelError("Room", "选择实验室安全卫生良好房间号为必填");
                    }
                    safeAndProduce.Score = 6;
                    break;
                default:
                    if (safeAndProduce.Amount.HasValue == false)
                    {
                        ModelState.AddModelError("Amount", "选择自制实验设备金额为必填"); break;
                    }
                    else if (safeAndProduce.Amount.Value <= 0)
                    {
                        ModelState.AddModelError("Amount", "选择自制实验设备金额为必填"); break;
                    }
                    safeAndProduce.Score = safeAndProduce.Amount.Value * 3;
                    break;
            }

            if (ModelState.IsValid)
            {
                db.Entry(safeAndProduce).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(ViewDirectory("Edit"), safeAndProduce);
        }

        // GET: SafeAndProduce/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            SafeAndProduce safeAndProduce = await db.SafeAndProduce.FindAsync(id);
            if (safeAndProduce == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (safeAndProduce.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限查看该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            return View(ViewDirectory("Delete"), safeAndProduce);
        }

        // POST: SafeAndProduce/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            SafeAndProduce safeAndProduce = await db.SafeAndProduce.FindAsync(id);
            db.SafeAndProduce.Remove(safeAndProduce);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
