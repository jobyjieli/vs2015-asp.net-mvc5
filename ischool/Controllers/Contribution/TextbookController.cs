﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ischool.Models;
using Microsoft.AspNet.Identity;

namespace ischool.Controllers
{
    [Authorize]
    public class TextbookController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        private string ViewDirectory(string nav)
        {
            string s = "~/Views/Contribution/Textbook/" + nav + ".cshtml";
            return s;
        }
        // GET: Textbook
        public async Task<ActionResult> Index()
        {
            var userId = User.Identity.GetUserId();
            var textbook = db.Textbook.Where(t => t.StaffInfoId == userId);
            return View(ViewDirectory("Index"), await textbook.ToListAsync());

        }

        // GET: Textbook/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            Textbook textbook = await db.Textbook.FindAsync(id);
            if (textbook == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (textbook.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限查看该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            return View(ViewDirectory("Details"), textbook);
        }

        // GET: Textbook/Create
        public ActionResult Create()
        {
            var userId = User.Identity.GetUserId();
            ViewBag.StaffInfoId = userId;
            return View(ViewDirectory("Create"));
        }

        // POST: Textbook/Create
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,StaffInfoId,Book,Press,Time,Type,Words,Score")] Textbook textbook)
        {
            var userId = User.Identity.GetUserId();
            var presstype = textbook.Type;
            ViewBag.StaffInfoId = userId;

            switch (presstype)
            {
                case PressType.全国高校通用统编教科书:
                    textbook.Score = (float)2.5 * textbook.Words;break;
                case PressType.一般教材:
                    textbook.Score = (float)1.8 * textbook.Words; break;
                default:
                    textbook.Score = textbook.Words; break;
            }
            if(textbook.Words <= 0)
            {
                ModelState.AddModelError("Words", "个人完成字数必须大于零");
            }

            if (ModelState.IsValid)
            {
                db.Textbook.Add(textbook);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(ViewDirectory("Create"), textbook);
        }

        // GET: Textbook/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            var userId = User.Identity.GetUserId();

            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            Textbook textbook = await db.Textbook.FindAsync(id);
            if (textbook == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage","Home");
            }
            else if (textbook.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限编辑该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            ViewBag.StaffInfoId = userId;
            return View(ViewDirectory("Edit"), textbook);
        }

        // POST: Textbook/Edit/5
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,StaffInfoId,Book,Press,Time,Type,Words,Score")] Textbook textbook)
        {
            var userId = User.Identity.GetUserId();
            var presstype = textbook.Type;
            ViewBag.StaffInfoId = userId;

            switch (presstype)
            {
                case PressType.全国高校通用统编教科书:
                    textbook.Score = (float)2.5 * textbook.Words; break;
                case PressType.一般教材:
                    textbook.Score = (float)1.8 * textbook.Words; break;
                default:
                    textbook.Score = textbook.Words; break;
            }
            if (textbook.Words <= 0)
            {
                ModelState.AddModelError("Words", "个人完成字数必须大于零");
            }

            if (ModelState.IsValid)
            {
                db.Entry(textbook).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(ViewDirectory("Edit"), textbook);
        }

        // GET: Textbook/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            Textbook textbook = await db.Textbook.FindAsync(id);
            if (textbook == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (textbook.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限删除该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            return View(ViewDirectory("Delete"), textbook);
        }

        // POST: Textbook/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Textbook textbook = await db.Textbook.FindAsync(id);
            db.Textbook.Remove(textbook);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
