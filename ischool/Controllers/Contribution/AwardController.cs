﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ischool.Models;
using Microsoft.AspNet.Identity;

namespace ischool.Controllers
{
    [Authorize]
    public class AwardController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        private string ViewDirectory(string nav)
        {
            string s = "~/Views/Contribution/Award/" + nav + ".cshtml";
            return s;
        }
        // GET: Award
        public async Task<ActionResult> Index()
        {
            var userId = User.Identity.GetUserId();
            var award = db.Award.Where(a => a.StaffInfoId == userId);
            return View(ViewDirectory("Index"),await award.ToListAsync());
        }

        // GET: Award/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            Award award = await db.Award.FindAsync(id);
            if (award == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (award.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限查看该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            return View(ViewDirectory("Details"),award);
        }

        // GET: Award/Create
        public ActionResult Create()
        {
            var userId = User.Identity.GetUserId();
            ViewBag.StaffInfoId = userId;
            return View(ViewDirectory("Create"));
        }

        // POST: Award/Create
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,StaffInfoId,Content,Time,Level,Score,TotalNum,Sequence,PersonalScore")] Award award)
        {
            var userId = User.Identity.GetUserId();
            var awardlevel = award.Level;
            ViewBag.StaffInfoId = userId;

            switch (awardlevel)
            {
                case Level.国家级:
                    award.Score = 60; break;
                case Level.省级:
                    award.Score = 40; break;
                case Level.校级:
                    award.Score = 20; break;
                default:
                    award.Score = 3; break;
            }
            if (award.TotalNum <= 0)
            {
                ModelState.AddModelError("TotalNum", "完成人员总数必须大于零");
            }
            if (award.Sequence <= 0)
            {
                ModelState.AddModelError("Sequence", "个人排名必须大于零");
            }
            else if(award.Sequence > award.TotalNum)
            {
                ModelState.AddModelError("Sequence", "个人排名不得大于完成人员总数");
            }

            if (ModelState.IsValid)
            {
                db.Award.Add(award);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(ViewDirectory("Create"),award);
        }

        // GET: Award/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            Award award = await db.Award.FindAsync(id);
            if (award == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (award.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限编辑该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            ViewBag.StaffInfoId = userId;
            return View(ViewDirectory("Edit"),award);
        }

        // POST: Award/Edit/5
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,StaffInfoId,Content,Time,Level,Score,TotalNum,Sequence,PersonalScore")] Award award)
        {
            var userId = User.Identity.GetUserId();
            var awardlevel = award.Level;
            ViewBag.StaffInfoId = userId;

            switch (awardlevel)
            {
                case Level.国家级:
                    award.Score = 60; break;
                case Level.省级:
                    award.Score = 40; break;
                case Level.校级:
                    award.Score = 20; break;
                default:
                    award.Score = 3; break;
            }
            if (award.TotalNum <= 0)
            {
                ModelState.AddModelError("TotalNum", "完成人员总数必须大于零");
            }
            if (award.Sequence <= 0)
            {
                ModelState.AddModelError("Sequence", "个人排名必须大于零");
            }
            else if (award.Sequence > award.TotalNum)
            {
                ModelState.AddModelError("Sequence", "个人排名不得大于完成人员总数");
            }

            if (ModelState.IsValid)
            {
                db.Entry(award).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(ViewDirectory("Edit"),award);
        }

        // GET: Award/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            Award award = await db.Award.FindAsync(id);
            if (award == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (award.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限删除该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            return View(ViewDirectory("Delete"),award);
        }

        // POST: Award/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Award award = await db.Award.FindAsync(id);
            db.Award.Remove(award);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
