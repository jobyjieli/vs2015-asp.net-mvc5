﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ischool.Models;
using Microsoft.AspNet.Identity;

namespace ischool.Controllers
{
    [Authorize]
    public class MaintenanceController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private string ViewDirectory(string nav)
        {
            string s = "~/Views/Contribution/Maintenance/" + nav + ".cshtml";
            return s;
        }
        // GET: Maintenance
        public async Task<ActionResult> Index()
        {
            var userId = User.Identity.GetUserId();
            var maintenance = db.Maintenance.Where(m => m.StaffInfoId == userId);
            return View(ViewDirectory("Index"), await maintenance.ToListAsync());
        }

        // GET: Maintenance/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            Maintenance maintenance = await db.Maintenance.FindAsync(id);
            if (maintenance == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (maintenance.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限查看该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            return View(ViewDirectory("Details"), maintenance);
        }

        // GET: Maintenance/Create
        public ActionResult Create()
        {
            var userId = User.Identity.GetUserId();
            ViewBag.StaffInfoId = userId;
            return View(ViewDirectory("Create"));
        }

        // POST: Maintenance/Create
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,StaffInfoId,Room,Time,Ratio,Score,PersonalScore")] Maintenance maintenance)
        {
            var userId = User.Identity.GetUserId();
            ViewBag.StaffInfoId = userId;
            if (DateTime.Now.Year - maintenance.Time.Year <= 5)
            {
                if (maintenance.Ratio == 100)
                {
                    maintenance.Score = 6;
                }
                else if (maintenance.Ratio >= 95)
                {
                    maintenance.Score = 3;
                }
                else if (maintenance.Ratio >= 90)
                {
                    maintenance.Score = (float)1.5;
                }
            }
            else if (DateTime.Now.Year - maintenance.Time.Year <= 8)
            {
                if (maintenance.Ratio >= 90)
                {
                    maintenance.Score = 6;
                }
                else if (maintenance.Ratio >= 85)
                {
                    maintenance.Score = 3;
                }
                else if (maintenance.Ratio >= 80)
                {
                    maintenance.Score = (float)1.5;
                }
            }
            else if (DateTime.Now.Year - maintenance.Time.Year <= 12)
            {
                if (maintenance.Ratio >= 80)
                {
                    maintenance.Score = 6;
                }
                else if (maintenance.Ratio >= 75)
                {
                    maintenance.Score = 3;
                }
                else if (maintenance.Ratio >= 70)
                {
                    maintenance.Score = (float)1.5;
                }
            }
//            else if (DateTime.Now.Year - maintenance.Time.Year <= 15)
            else
            {
                if (maintenance.Ratio >= 70)
                {
                    maintenance.Score = 6;
                }
                else if (maintenance.Ratio >= 65)
                {
                    maintenance.Score = 3;
                }
                else if (maintenance.Ratio >= 60)
                {
                    maintenance.Score = (float)1.5;
                }
            }

            if (ModelState.IsValid)
            {
                db.Maintenance.Add(maintenance);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(ViewDirectory("Create"), maintenance);
        }

        // GET: Maintenance/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            Maintenance maintenance = await db.Maintenance.FindAsync(id);
            if (maintenance == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (maintenance.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限查看该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            ViewBag.StaffInfoId = userId;
            return View(ViewDirectory("Edit"), maintenance);
        }

        // POST: Maintenance/Edit/5
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,StaffInfoId,Room,Time,Ratio,Score,PersonalScore")] Maintenance maintenance)
        {
            var userId = User.Identity.GetUserId();
            ViewBag.StaffInfoId = userId;
            if (DateTime.Now.Year - maintenance.Time.Year <= 5)
            {
                if (maintenance.Ratio == 100)
                {
                    maintenance.Score = 6;
                }
                else if (maintenance.Ratio >= 95)
                {
                    maintenance.Score = 3;
                }
                else if (maintenance.Ratio >= 90)
                {
                    maintenance.Score = (float)1.5;
                }
            }
            else if (DateTime.Now.Year - maintenance.Time.Year <= 8)
            {
                if (maintenance.Ratio >= 90)
                {
                    maintenance.Score = 6;
                }
                else if (maintenance.Ratio >= 85)
                {
                    maintenance.Score = 3;
                }
                else if (maintenance.Ratio >= 80)
                {
                    maintenance.Score = (float)1.5;
                }
            }
            else if (DateTime.Now.Year - maintenance.Time.Year <= 12)
            {
                if (maintenance.Ratio >= 80)
                {
                    maintenance.Score = 6;
                }
                else if (maintenance.Ratio >= 75)
                {
                    maintenance.Score = 3;
                }
                else if (maintenance.Ratio >= 70)
                {
                    maintenance.Score = (float)1.5;
                }
            }
            //            else if (DateTime.Now.Year - maintenance.Time.Year <= 15)
            else
            {
                if (maintenance.Ratio >= 70)
                {
                    maintenance.Score = 6;
                }
                else if (maintenance.Ratio >= 65)
                {
                    maintenance.Score = 3;
                }
                else if (maintenance.Ratio >= 60)
                {
                    maintenance.Score = (float)1.5;
                }
            }

            if (ModelState.IsValid)
            {
                db.Entry(maintenance).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(ViewDirectory("Edit"), maintenance);
        }

        // GET: Maintenance/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            Maintenance maintenance = await db.Maintenance.FindAsync(id);
            if (maintenance == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (maintenance.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限查看该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            return View(ViewDirectory("Delete"), maintenance);
        }

        // POST: Maintenance/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Maintenance maintenance = await db.Maintenance.FindAsync(id);
            db.Maintenance.Remove(maintenance);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
