﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ischool.Models;
using Microsoft.AspNet.Identity;

namespace ischool.Controllers
{
    [Authorize]
    public class ProjectController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private string ViewDirectory(string nav)
        {
            string s = "~/Views/Contribution/Project/" + nav + ".cshtml";
            return s;
        }
        // GET: Project
        public async Task<ActionResult> Index()
        {
            var userId = User.Identity.GetUserId();
            var project = db.Project.Where(a => a.StaffInfoId == userId);
            return View(ViewDirectory("Index"), await project.ToListAsync());
        }

        // GET: Project/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            Project project = await db.Project.FindAsync(id);
            if (project == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (project.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限查看该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            return View(ViewDirectory("Details"), project);
        }

        // GET: Project/Create
        public ActionResult Create()
        {
            var userId = User.Identity.GetUserId();
            ViewBag.StaffInfoId = userId;
            return View(ViewDirectory("Create"));
        }

        // POST: Project/Create
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,StaffInfoId,Content,Time,Type,Score,TotalNum,Sequence,PersonalScore")] Project project)
        {
            var userId = User.Identity.GetUserId();
            var projectType = project.Type;
            ViewBag.StaffInfoId = userId;
            switch (projectType)
            {
                case Level.国家级:
                    project.Score = 10; break;
                case Level.省级:
                    project.Score = 7; break;
                case Level.校级:
                    project.Score = 4; break;
                default:
                    project.Score = 2; break;
            }
            if (project.TotalNum <= 0)
            {
                ModelState.AddModelError("TotalNum", "完成人员总数必须大于零");
            }
            if (project.Sequence <= 0)
            {
                ModelState.AddModelError("Sequence", "个人排名必须大于零");
            }
            else if (project.Sequence > project.TotalNum)
            {
                ModelState.AddModelError("Sequence", "个人排名不得大于完成人员总数");
            }

            if (ModelState.IsValid)
            {
                db.Project.Add(project);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(ViewDirectory("Create"), project);
        }

        // GET: Project/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            Project project = await db.Project.FindAsync(id);
            if (project == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (project.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限查看该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            ViewBag.StaffInfoId = userId;
            return View(ViewDirectory("Edit"), project);
        }

        // POST: Project/Edit/5
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,StaffInfoId,Content,Time,Type,Score,TotalNum,Sequence,PersonalScore")] Project project)
        {
            var userId = User.Identity.GetUserId();
            var projectType = project.Type;
            ViewBag.StaffInfoId = userId;
            switch (projectType)
            {
                case Level.国家级:
                    project.Score = 10; break;
                case Level.省级:
                    project.Score = 7; break;
                case Level.校级:
                    project.Score = 4; break;
                default:
                    project.Score = 2; break;
            }
            if (project.TotalNum <= 0)
            {
                ModelState.AddModelError("TotalNum", "完成人员总数必须大于零");
            }
            if (project.Sequence <= 0)
            {
                ModelState.AddModelError("Sequence", "个人排名必须大于零");
            }
            else if (project.Sequence > project.TotalNum)
            {
                ModelState.AddModelError("Sequence", "个人排名不得大于完成人员总数");
            }

            if (ModelState.IsValid)
            {
                db.Entry(project).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(ViewDirectory("Edit"), project);
        }

        // GET: Project/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            Project project = await db.Project.FindAsync(id);
            if (project == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (project.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限查看该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            return View(ViewDirectory("Delete"), project);
        }

        // POST: Project/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Project project = await db.Project.FindAsync(id);
            db.Project.Remove(project);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
