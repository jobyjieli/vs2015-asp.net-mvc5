﻿using ischool.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ischool.Controllers
{

    [DisplayName("贡献相关")]
    public class ContributionModel
    {
        public Textbook Textbook { get; }
        public Award Award { get; }
        public CourseDevelop CourseDevelop { get; }
        public TeacherAward TeacherAward { get; }
        public TeacherSkill TeacherSkill { get; }
        public Paper Paper { get; }
        public Competition Competition { get; }
        public Project Project { get; }
        public Research Research { get; }
        public Fund Fund { get; }
        public Patent Patent { get; }
        public Serve Serve { get; }
        public Construct Construct { get; }
        public SafeAndProduce SafeAndProduce { get; }
        public Maintenance Maintenance { get; }
        public Charge Charge { get; }
        public OtherContribute OtherContribute { get; }


        public List<Textbook> TextbookModel { get; set; }
        public List<Award> AwardModel { get; set; }
        public List<CourseDevelop> CourseDevelopModel { get; set; }
        public List<TeacherAward> TeacherAwardModel { get; set; }
        public List<TeacherSkill> TeacherSkillModel { get; set; }
        public List<Paper> PaperModel { get; set; }
        public List<Competition> CompetitionModel { get; set; }
        public List<Project> ProjectModel { get; set; }
        public List<Research> ResearchModel { get; set; }
        public List<Fund> FundModel { get; set; }
        public List<Patent> PatentModel { get; set; }
        public List<Serve> ServeModel { get; set; }
        public List<Construct> ConstructModel { get; set; }
        public List<SafeAndProduce> SafeAndProduceModel { get; set; }
        public List<Maintenance> MaintenanceModel { get; set; }
        public List<Charge> ChargeModel { get; set; }
        public List<OtherContribute> OtherContributeModel { get; set; }


        public ContributionModel(List<Models.Textbook> TextbookList, List<Models.Award> AwardList,List<Models.CourseDevelop> CourseDevelopList,
            List<Models.TeacherAward> TeacherAwardList, List<Models.TeacherSkill> TeacherSkillList, List<Models.Paper> PaperList, 
            List<Models.Competition> CompetitionList, List<Models.Project> ProjectList, List<Models.Research> ResearchList, 
            List<Models.Fund> FundList, List<Models.Patent> PatentList, List<Models.Serve> ServeList, List<Models.Construct> ConstructList, 
            List<Models.SafeAndProduce> SafeAndProduceList, List<Models.Maintenance> MaintenanceList, List<Models.Charge> ChargeList, List<Models.OtherContribute> OtherContributeList)
        {
            this.TextbookModel = TextbookList;
            this.AwardModel = AwardList;
            this.CourseDevelopModel = CourseDevelopList;
            this.TeacherAwardModel = TeacherAwardList;
            this.TeacherSkillModel = TeacherSkillList;
            this.PaperModel = PaperList;
            this.CompetitionModel = CompetitionList;
            this.ProjectModel = ProjectList;
            this.ResearchModel = ResearchList;
            this.FundModel = FundList;
            this.PatentModel = PatentList;
            this.ServeModel = ServeList;
            this.ConstructModel = ConstructList;
            this.SafeAndProduceModel = SafeAndProduceList;
            this.MaintenanceModel = MaintenanceList;
            this.ChargeModel = ChargeList;
            this.OtherContributeModel = OtherContributeList;
        }
    }


    [Authorize]
    public class ContributionController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: Contribution
        public async Task<ActionResult> Index()
        {
            var userId = User.Identity.GetUserId();
            StaffInfo staffInfo = await db.StaffInfo.FindAsync(userId);
            if (staffInfo == null)
            {
                return RedirectToAction("InfoCreate", "Account");
            }
            var vm = new ContributionModel(db.Textbook.ToList(), db.Award.ToList(), db.CourseDevelop.ToList(), db.TeacherAward.ToList(), db.TeacherSkill.ToList(), db.Paper.ToList(),
                db.Competition.ToList(), db.Project.ToList(), db.Research.ToList(), db.Fund.ToList(), db.Patent.ToList(), db.Serve.ToList(), db.Construct.ToList(), db.SafeAndProduce.ToList(), 
                db.Maintenance.ToList(), db.Charge.ToList(), db.OtherContribute.ToList());
            vm.TextbookModel = await db.Textbook.Where(t => t.StaffInfoId == userId).ToListAsync();
            vm.AwardModel = await db.Award.Where(t => t.StaffInfoId == userId).ToListAsync();
            vm.CourseDevelopModel = await db.CourseDevelop.Where(t => t.StaffInfoId == userId).ToListAsync();
            vm.TeacherAwardModel = await db.TeacherAward.Where(t => t.StaffInfoId == userId).ToListAsync();
            vm.TeacherSkillModel = await db.TeacherSkill.Where(t => t.StaffInfoId == userId).ToListAsync();
            vm.PaperModel = await db.Paper.Where(t => t.StaffInfoId == userId).ToListAsync();
            vm.CompetitionModel = await db.Competition.Where(t => t.StaffInfoId == userId).ToListAsync();
            vm.ProjectModel = await db.Project.Where(t => t.StaffInfoId == userId).ToListAsync();
            vm.ResearchModel = await db.Research.Where(t => t.StaffInfoId == userId).ToListAsync();
            vm.FundModel = await db.Fund.Where(t => t.StaffInfoId == userId).ToListAsync();
            vm.PatentModel = await db.Patent.Where(t => t.StaffInfoId == userId).ToListAsync();
            vm.ServeModel = await db.Serve.Where(t => t.StaffInfoId == userId).ToListAsync();
            vm.ConstructModel = await db.Construct.Where(t => t.StaffInfoId == userId).ToListAsync();
            vm.SafeAndProduceModel = await db.SafeAndProduce.Where(t => t.StaffInfoId == userId).ToListAsync();
            vm.MaintenanceModel = await db.Maintenance.Where(t => t.StaffInfoId == userId).ToListAsync();
            vm.ChargeModel = await db.Charge.Where(t => t.StaffInfoId == userId).ToListAsync();
            vm.OtherContributeModel = await db.OtherContribute.Where(t => t.StaffInfoId == userId).ToListAsync();

            return View(vm);
        }
    }
}