﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ischool.Models;
using Microsoft.AspNet.Identity;

namespace ischool.Controllers
{
    [Authorize]
    public class TeacherAwardController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private string ViewDirectory(string nav)
        {
            string s = "~/Views/Contribution/TeacherAward/" + nav + ".cshtml";
            return s;
        }
        // GET: TeacherAward
        public async Task<ActionResult> Index()
        {
            var userId = User.Identity.GetUserId();
            var teacherAward = db.TeacherAward.Where(t => t.StaffInfoId == userId);
            return View(ViewDirectory("Index"), await teacherAward.ToListAsync());
        }

        // GET: TeacherAward/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            TeacherAward teacherAward = await db.TeacherAward.FindAsync(id);
            if (teacherAward == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (teacherAward.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限查看该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            return View(ViewDirectory("Details"), teacherAward);
        }

        // GET: TeacherAward/Create
        public ActionResult Create()
        {
            var userId = User.Identity.GetUserId();
            ViewBag.StaffInfoId = userId;
            return View(ViewDirectory("Create"));
        }

        // POST: TeacherAward/Create
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,StaffInfoId,Time,Level,Score")] TeacherAward teacherAward)
        {
            var userId = User.Identity.GetUserId();
            var teacherAwardlevel = teacherAward.Level;
            ViewBag.StaffInfoId = userId;

            switch (teacherAwardlevel)
            {
                case Level.国家级:
                    teacherAward.Score = 50; break;
                case Level.省级:
                    teacherAward.Score = 30; break;
                case Level.校级:
                    teacherAward.Score = 15; break;
                default:
                    teacherAward.Score = 3; break;
            }

            if (ModelState.IsValid)
            {
                db.TeacherAward.Add(teacherAward);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(ViewDirectory("Create"), teacherAward);
        }

        // GET: TeacherAward/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            TeacherAward teacherAward = await db.TeacherAward.FindAsync(id);
            if (teacherAward == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (teacherAward.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限查看该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            ViewBag.StaffInfoId = userId;
            return View(ViewDirectory("Edit"), teacherAward);
        }

        // POST: TeacherAward/Edit/5
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,StaffInfoId,Time,Level,Score")] TeacherAward teacherAward)
        {
            var userId = User.Identity.GetUserId();
            var teacherAwardlevel = teacherAward.Level;
            ViewBag.StaffInfoId = userId;

            switch (teacherAwardlevel)
            {
                case Level.国家级:
                    teacherAward.Score = 50; break;
                case Level.省级:
                    teacherAward.Score = 30; break;
                case Level.校级:
                    teacherAward.Score = 15; break;
                default:
                    teacherAward.Score = 3; break;
            }

            if (ModelState.IsValid)
            {
                db.Entry(teacherAward).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(ViewDirectory("Edit"), teacherAward);
        }

        // GET: TeacherAward/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            var userId = User.Identity.GetUserId();
            if (id == null)
            {
                TempData["ErrorMessage"] = "条目Id出错";
                return RedirectToAction("ErrorMessage", "Home");
            }
            TeacherAward teacherAward = await db.TeacherAward.FindAsync(id);
            if (teacherAward == null)
            {
                TempData["ErrorMessage"] = "未找到指定的条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            else if (teacherAward.StaffInfoId != userId)
            {
                TempData["ErrorMessage"] = "您没有权限查看该条目";
                return RedirectToAction("ErrorMessage", "Home");
            }
            return View(ViewDirectory("Delete"), teacherAward);
        }

        // POST: TeacherAward/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            TeacherAward teacherAward = await db.TeacherAward.FindAsync(id);
            db.TeacherAward.Remove(teacherAward);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
