﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ischool.Models;

namespace ischool.Controllers
{
    public class StaffInfoController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: StaffInfo
        public ActionResult Index()
        {
            return View(db.StaffInfo.ToList());
        }

        // GET: StaffInfo/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StaffInfo staffInfo = db.StaffInfo.Find(id);
            if (staffInfo == null)
            {
                return HttpNotFound();
            }
            return View(staffInfo);
        }

        // GET: StaffInfo/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: StaffInfo/Create
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Gender,Birth,Platform,Lab,Room,Position,Title,titleDate,Education,Academy,Degree,employTime,Tel,Mobile,profile,remark")] StaffInfo staffInfo)
        {
            if (ModelState.IsValid)
            {
                db.StaffInfo.Add(staffInfo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(staffInfo);
        }

        // GET: StaffInfo/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StaffInfo staffInfo = db.StaffInfo.Find(id);
            if (staffInfo == null)
            {
                return HttpNotFound();
            }
            return View(staffInfo);
        }

        // POST: StaffInfo/Edit/5
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Gender,Birth,Platform,Lab,Room,Position,Title,titleDate,Education,Academy,Degree,employTime,Tel,Mobile,profile,remark")] StaffInfo staffInfo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(staffInfo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(staffInfo);
        }

        // GET: StaffInfo/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StaffInfo staffInfo = db.StaffInfo.Find(id);
            if (staffInfo == null)
            {
                return HttpNotFound();
            }
            return View(staffInfo);
        }

        // POST: StaffInfo/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            StaffInfo staffInfo = db.StaffInfo.Find(id);
            db.StaffInfo.Remove(staffInfo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
