﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ischool.Models
{
    public enum PressType
    {
        全国高校通用统编教科书,一般教材,实验讲义
    }

    public enum Level
    {
        国家级, 省级, 校级, 申报未果
    }

    public enum Level2
    {
        国家级, 省级, 校级
    }

    public enum Prize
    {
        特等奖,一等奖,二等奖, 三等奖,校级申报未取
    }

    public enum PaperType
    {
        SCI刊物,EI刊物,核心期刊,一般论文及会议论文
    }

    public enum CompetitionType
    {
        本学科有重大影响的国际或国家级,本学科有影响的省部级,校级竞赛
    }

    public enum CompetitionAward
    {
        特等奖, 一等奖, 二等奖, 三等奖, 有名次
    }

    public enum ResearchLevel
    {
        国家级教改及科研,省级教改及科研,校级教改,院级教改,申报未取
    }

    public enum PatentType
    {
        国家发明专利,软件著作权,实用新型专利
    }
    public enum ServeType
    {
        创收服务或中心可支配留成经费,校外企业赞助学生竞赛,校外企业赞助学术会议
    }
    public enum ConstructType
    {
        实验室硬件建设,系统软件立项建设,引入联合实验室设备或软件
    }
    public enum SafeAndProduceType
    {
        实验室安全卫生良好,自制实验设备
    }
    public enum DutyType
    {
        系中心副主任或书记,平台主任,平台副主任,校外重要学术团体兼任职务,公用网站网络软件维护
    }


    [DisplayName("教材")]
    public class Textbook
    {
        [Required]
        public int Id { get; set; }

        [HiddenInput(DisplayValue = false)]
        [DisplayName("工号")]
        public string StaffInfoId { get; set; }

        [Required]
        [DisplayName("书名")]
        public string Book { get; set; }  

        [Required]
        [DisplayName("出版社")]
        public string Press { get; set; }

        [Required]
        [DisplayName("出版年月")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM}", ApplyFormatInEditMode = true)]
        public DateTime Time { get; set; }

        [Required]
        [DisplayName("类别")]
        public PressType?Type { get; set; }

        [Required]
        [DisplayName("个人完成字数（万字）")]
        public float Words { get; set; }
       
        [DisplayName("个人得分")]
        public float Score { get; set; }    

 
        public virtual StaffInfo StaffInfo { get; set; }

    }



    [DisplayName("教学及科研成果奖")]
    public class Award
    {
        [Required]
        public int Id { get; set; }

        [HiddenInput(DisplayValue = false)]
        [DisplayName("工号")]
        public string StaffInfoId { get; set; }

        [Required]
        [DisplayName("获奖成果名称")]
        public string Content { get; set; }

        [Required]
        [DisplayName("获奖时间")]
        [DataType(DataType.Date)]
        public DateTime Time { get; set; }

        [Required]
        [DisplayName("级别")]
        public Level? Level { get; set; }

        [DisplayName("分值")]
        public float Score { get; set; }

        [Required]
        [DisplayName("完成人员总数")]
        public int TotalNum { get; set; }

        [Required]
        [DisplayName("个人排名")]
        public int Sequence { get; set; }

        [DisplayName("个人得分")]
        public float PersonalScore { get; set; }


        public virtual StaffInfo StaffInfo { get; set; }

    }


    [DisplayName("课程建设")]
    public class CourseDevelop
    {
        [Required]
        public int Id { get; set; }

        [HiddenInput(DisplayValue = false)]
        [DisplayName("工号")]
        public string StaffInfoId { get; set; }

        [Required]
        [DisplayName("课程建设内容")]
        public string Content { get; set; }

        [Required]
        [DisplayName("时间")]
        [DataType(DataType.Date)]
        public DateTime Time { get; set; }

        [Required]
        [DisplayName("级别")]
        public Level? Level { get; set; }

        [DisplayName("分值")]
        public float Score { get; set; }

        [Required]
        [DisplayName("完成人员总数")]
        public int TotalNum { get; set; }

        [Required]
        [DisplayName("个人排名")]
        public int Sequence { get; set; }

        [DisplayName("个人得分")]
        public float PersonalScore { get; set; }


        public virtual StaffInfo StaffInfo { get; set; }

    }



    [DisplayName("教学名师")]
    public class TeacherAward
    {
        [Required]
        public int Id { get; set; }

        [HiddenInput(DisplayValue = false)]
        [DisplayName("工号")]
        public string StaffInfoId { get; set; }

        [Required]
        [DisplayName("时间")]
        [DataType(DataType.Date)]
        public DateTime Time { get; set; }

        [Required]
        [DisplayName("级别")]
        public Level? Level { get; set; }

        [DisplayName("分值")]
        public float Score { get; set; }


        public virtual StaffInfo StaffInfo { get; set; }

    }



    [DisplayName("教师技能竞赛")]
    public class TeacherSkill
    {
        [Required]
        public int Id { get; set; }

        [HiddenInput(DisplayValue = false)]
        [DisplayName("工号")]
        public string StaffInfoId { get; set; }

        [Required]
        [DisplayName("教师技能竞赛名称")]
        public string Content { get; set; }

        [Required]
        [DisplayName("时间")]
        [DataType(DataType.Date)]
        public DateTime Time { get; set; }

        [Required]
        [DisplayName("级别")]
        public Level2? Level { get; set; }


        [Required]
        [DisplayName("奖项")]
        public Prize? Award { get; set; }


        [DisplayName("分值")]
        public float Score { get; set; }


        public virtual StaffInfo StaffInfo { get; set; }

    }



    [DisplayName("论文")]
    public class Paper
    {
        [Required]
        public int Id { get; set; }

        [HiddenInput(DisplayValue = false)]
        [DisplayName("工号")]
        public string StaffInfoId { get; set; }

        [Required]
        [DisplayName("论文题目")]
        public string Title { get; set; }

        [Required]
        [DisplayName("刊物名称")]
        public string Journal { get; set; }

        [Required]
        [DisplayName("发表时间")]
        [DataType(DataType.Date)]
        public DateTime Time { get; set; }

        [Required]
        [DisplayName("论文类型")]
        public PaperType? Type { get; set; }

        [DisplayName("分值")]
        public float Score { get; set; }

        [Required]
        [DisplayName("完成人员总数")]
        public int TotalNum { get; set; }

        [Required]
        [DisplayName("个人排名")]
        public int Sequence { get; set; }

        [DisplayName("个人得分")]
        public float PersonalScore { get; set; }


        public virtual StaffInfo StaffInfo { get; set; }

    }



    [DisplayName("指导竞赛")]
    public class Competition
    {
        [Required]
        public int Id { get; set; }

        [HiddenInput(DisplayValue = false)]
        [DisplayName("工号")]
        public string StaffInfoId { get; set; }

        [Required]
        [DisplayName("竞赛名称")]
        public string Content { get; set; }

        [Required]
        [DisplayName("时间")]
        [DataType(DataType.Date)]
        public DateTime Time { get; set; }

        [Required]
        [DisplayName("类别")]
        public CompetitionType? Type { get; set; }

        [Required]
        [DisplayName("奖项")]
        public CompetitionAward? Award { get; set; }


        [DisplayName("分值")]
        public float Score { get; set; }

        [Required]
        [DisplayName("完成人员总数")]
        public int TotalNum { get; set; }

        [Required]
        [DisplayName("个人排名")]
        public int Sequence { get; set; }

        [DisplayName("个人得分")]
        public float PersonalScore { get; set; }


        public virtual StaffInfo StaffInfo { get; set; }

    }




    [DisplayName("科技立项")]
    public class Project
    {
        [Required]
        public int Id { get; set; }

        [HiddenInput(DisplayValue = false)]
        [DisplayName("工号")]
        public string StaffInfoId { get; set; }

        [Required]
        [DisplayName("科技立项名称")]
        public string Content { get; set; }

        [Required]
        [DisplayName("时间")]
        [DataType(DataType.Date)]
        public DateTime Time { get; set; }

        [Required]
        [DisplayName("类别")]
        public Level? Type { get; set; }

        [DisplayName("分值")]
        public float Score { get; set; }

        [Required]
        [DisplayName("完成人员总数")]
        public int TotalNum { get; set; }

        [Required]
        [DisplayName("个人排名")]
        public int Sequence { get; set; }

        [DisplayName("个人得分")]
        public float PersonalScore { get; set; }


        public virtual StaffInfo StaffInfo { get; set; }

    }



    [DisplayName("主持或参与教改及科研项目")]
    public class Research
    {
        [Required]
        public int Id { get; set; }

        [HiddenInput(DisplayValue = false)]
        [DisplayName("工号")]
        public string StaffInfoId { get; set; }

        [Required]
        [DisplayName("教改及科研项目名称")]
        public string Content { get; set; }

        [Required]
        [DisplayName("时间")]
        [DataType(DataType.Date)]
        public DateTime Time { get; set; }

        [Required]
        [DisplayName("级别")]
        public ResearchLevel? Level { get; set; }

        [DisplayName("分值")]
        public float Score { get; set; }

        [Required]
        [DisplayName("完成人员总数")]
        public int TotalNum { get; set; }

        [Required]
        [DisplayName("个人排名")]
        public int Sequence { get; set; }

        [DisplayName("个人得分")]
        public float PersonalScore { get; set; }


        public virtual StaffInfo StaffInfo { get; set; }


    }



    [DisplayName("实验中心横向科研可提成经费")]
    public class Fund
    {
        [Required]
        public int Id { get; set; }

        [HiddenInput(DisplayValue = false)]
        [DisplayName("工号")]
        public string StaffInfoId { get; set; }

        [Required]
        [DisplayName("内容")]
        public string Content { get; set; }

        [Required]
        [DisplayName("时间")]
        [DataType(DataType.Date)]
        public DateTime Time { get; set; }

        [Required]
        [DisplayName("中心横向科研可提成经费金额（万元）")]
        public float Amount { get; set; }

        [DisplayName("分值")]
        public float Score { get; set; }

        [Required]
        [DisplayName("完成人员总数")]
        public int TotalNum { get; set; }

        [Required]
        [DisplayName("个人排名")]
        public int Sequence { get; set; }

        [DisplayName("个人得分")]
        public float PersonalScore { get; set; }


        public virtual StaffInfo StaffInfo { get; set; }


    }



    [DisplayName("专利")]
    public class Patent
    {
        [Required]
        public int Id { get; set; }

        [HiddenInput(DisplayValue = false)]
        [DisplayName("工号")]
        public string StaffInfoId { get; set; }

        [Required]
        [DisplayName("专利名称")]
        public string Name { get; set; }

        [Required]
        [DisplayName("时间")]
        [DataType(DataType.Date)]
        public DateTime Time { get; set; }

        [Required]
        [DisplayName("类别")]
        public PatentType? Type { get; set; }

        [DisplayName("分值")]
        public float Score { get; set; }

        [Required]
        [DisplayName("完成人员总数")]
        public int TotalNum { get; set; }

        [Required]
        [DisplayName("个人排名")]
        public int Sequence { get; set; }

        [DisplayName("个人得分")]
        public float PersonalScore { get; set; }


        public virtual StaffInfo StaffInfo { get; set; }


    }



    [DisplayName("对外辐射与服务")]
    public class Serve
    {
        [Required]
        public int Id { get; set; }

        [HiddenInput(DisplayValue = false)]
        [DisplayName("工号")]
        public string StaffInfoId { get; set; }

        [Required]
        [DisplayName("类别")]
        public ServeType? Type { get; set; }

        [Required]
        [DisplayName("内容")]
        public string Content { get; set; }

        [Required]
        [DisplayName("时间")]
        [DataType(DataType.Date)]
        public DateTime Time { get; set; }

        [Required]
        [DisplayName("金额（万元）")]
        public float Amount { get; set; }

        [DisplayName("分值")]
        public float Score { get; set; }

        [Required]
        [DisplayName("完成人员总数")]
        public int TotalNum { get; set; }

        [Required]
        [DisplayName("个人排名")]
        public int Sequence { get; set; }

        [DisplayName("个人得分")]
        public float PersonalScore { get; set; }


        public virtual StaffInfo StaffInfo { get; set; }


    }



    [DisplayName("实验室建设")]
    public class Construct
    {
        [Required]
        public int Id { get; set; }

        [HiddenInput(DisplayValue = false)]
        [DisplayName("工号")]
        public string StaffInfoId { get; set; }

        [Required]
        [DisplayName("类别")]
        public ConstructType? Type { get; set; }

        [Required]
        [DisplayName("内容")]
        public string Content { get; set; }

        [Required]
        [DisplayName("时间")]
        [DataType(DataType.Date)]
        public DateTime Time { get; set; }

        [Required]
        [DisplayName("金额（万元）")]
        public float Amount { get; set; }

        [DisplayName("分值")]
        public float Score { get; set; }

        [Required]
        [DisplayName("完成人员总数")]
        public int TotalNum { get; set; }

        [Required]
        [DisplayName("个人排名")]
        public int Sequence { get; set; }

        [DisplayName("个人得分")]
        public float PersonalScore { get; set; }


        public virtual StaffInfo StaffInfo { get; set; }


    }



    [DisplayName("实验室安全卫生、自制实验设备")]
    public class SafeAndProduce
    {
        [Required]
        public int Id { get; set; }

        [HiddenInput(DisplayValue = false)]
        [DisplayName("工号")]
        public string StaffInfoId { get; set; }

        [Required]
        [DisplayName("类别")]
        public SafeAndProduceType? Type { get; set; }

        [DisplayName("实验室安全卫生房间号")]
        public string Room { get; set; }
        
        [DisplayName("自制实验设备金额（万元）")]
        public float? Amount { get; set; }

        [Required]
        [DisplayName("时间")]
        [DataType(DataType.Date)]
        public DateTime Time { get; set; }

        [DisplayName("分值")]
        public float Score { get; set; }

        [DisplayName("个人得分")]
        public float PersonalScore { get; set; }


        public virtual StaffInfo StaffInfo { get; set; }


    }



    [DisplayName("设备维护")]
    public class Maintenance
    {
        [Required]
        public int Id { get; set; }

        [HiddenInput(DisplayValue = false)]
        [DisplayName("工号")]
        public string StaffInfoId { get; set; }

        [Required]
        [DisplayName("房间号")]
        public string Room { get; set; }

        [Required]
        [DisplayName("建设时间")]
        [DataType(DataType.Date)]
        public DateTime Time { get; set; }

        [Required]
        [DisplayName("利用率%")]
        [Range(0,100)]
        public float Ratio { get; set; }

        [DisplayName("分值")]
        public float Score { get; set; }

        [DisplayName("个人得分")]
        public float PersonalScore { get; set; }


        public virtual StaffInfo StaffInfo { get; set; }


    }



    [DisplayName("担任或兼任党政职务")]
    public class Charge
    {
        [Required]
        public int Id { get; set; }

        [HiddenInput(DisplayValue = false)]
        [DisplayName("工号")]
        public string StaffInfoId { get; set; }

        [Required]
        [DisplayName("职务名称")]
        public string Title { get; set; }

        [Required]
        [DisplayName("职务类型")]
        public DutyType? Duty { get; set; }

        [DisplayName("分值")]
        public float Score { get; set; }


        public virtual StaffInfo StaffInfo { get; set; }


    }
    


    [DisplayName("其他贡献")]
    public class OtherContribute
    {
        [Required]
        public int Id { get; set; }

        [HiddenInput(DisplayValue = false)]
        [DisplayName("工号")]
        public string StaffInfoId { get; set; }

        [Required]
        [DisplayName("内容")]
        [UIHint("MultilineText")]
        public string Content { get; set; }

        [Required]
        [DisplayName("时间")]
        [DataType(DataType.Date)]
        public DateTime Time { get; set; }

        [DisplayName("分值")]
        public float Score { get; set; }


        public virtual StaffInfo StaffInfo { get; set; }


    }


}