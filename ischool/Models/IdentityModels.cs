﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace ischool.Models
{
    // 可以通过向 ApplicationUser 类添加更多属性来为用户添加配置文件数据。若要了解详细信息，请访问 http://go.microsoft.com/fwlink/?LinkID=317594。
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // 请注意，authenticationType 必须与 CookieAuthenticationOptions.AuthenticationType 中定义的相应项匹配
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // 在此处添加自定义用户声明
            return userIdentity;
        }

        [Required]
        override public string Id { get; set; }

        public virtual StaffInfo StaffInfo { get; set; }

    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<StaffInfo> StaffInfo { get; set; }
        public DbSet<Textbook> Textbook { get; set; }
        public DbSet<Award> Award { get; set; }
        public DbSet<CourseDevelop> CourseDevelop { get; set; }
        public DbSet<TeacherAward> TeacherAward { get; set; }
        public DbSet<TeacherSkill> TeacherSkill { get; set; }
        public DbSet<Paper> Paper { get; set; }
        public DbSet<Competition> Competition { get; set; }
        public DbSet<Project> Project { get; set; }
        public DbSet<Research> Research { get; set; }
        public DbSet<Fund> Fund { get; set; }
        public DbSet<Patent> Patent { get; set; }
        public DbSet<Serve> Serve { get; set; }
        public DbSet<Construct> Construct { get; set; }
        public DbSet<SafeAndProduce> SafeAndProduce { get; set; }
        public DbSet<Maintenance> Maintenance { get; set; }
        public DbSet<Charge> Charge { get; set; }
        public DbSet<OtherContribute> OtherContribute { get; set; }

    }


    public enum Gender
    {
        男,女
    }

    [DisplayName("员工信息")]
    public class StaffInfo
    {
        [ForeignKey("ApplicationUser")]
        public string Id { get; set; }

        [Required]
        [DisplayName("姓名")]
        public string Name { get; set; }    //姓名

        [Required]
        [DisplayName("性别")]
        public Gender? Gender { get; set; }  //性别

        [Required]
        [DisplayName("出生日期")]
        [DataType(DataType.Date)]
        public DateTime Birth { get; set; }   //出生日期

        [Required]
        [DisplayName("所属平台")]
        public string Platform { get; set; }    //所属平台

        [Required]
        [DisplayName("所属实验室")]
        public string Lab { get; set; }     //所属实验室

        [DisplayName("房间")]
        public string Room { get; set; }    //房间

        [Required]
        [DisplayName("岗位")]
        public string Position { get; set; }    //岗位

        [Required]
        [DisplayName("职称")]
        public string Title { get; set; }    //职称

        [Required]
        [DisplayName("任职时间")]
        [DataType(DataType.Date)]
        public DateTime titleDate { get; set; }    //任职时间

        [DisplayName("学历")]
        public string Education { get; set; }    //学历

        [DisplayName("最高学历院校")]
        public string Academy { get; set; }    //最高学历院校

        [DisplayName("学位")]
        public string Degree { get; set; }    //学位

        [DisplayName("来校工作时间")]
        [DataType(DataType.Date)]
        public DateTime employTime { get; set; }    //来校工作时间

        [DisplayName("办公电话")]
        public string Tel { get; set; }     //办公电话
  
        [Required]
        [DisplayName("手机")]
        public string Mobile { get; set; }     //手机

        [Required]
        [DisplayName("个人简介")]
        [UIHint("MultilineText")]
        public string profile { get; set; }     //个人简介

        [Required]
        [DisplayName("备注")]
        [UIHint("MultilineText")]
        public string remark { get; set; }     //备注


        public virtual ApplicationUser ApplicationUser { get; set; }


        public virtual ICollection<Textbook> Textbook { get; set; }
        public virtual ICollection<Award> Award { get; set; }
        public virtual ICollection<CourseDevelop> CourseDevelop { get; set; }
        public virtual ICollection<TeacherAward> TeacherAward { get; set; }
        public virtual ICollection<TeacherSkill> TeacherSkill { get; set; }
        public virtual ICollection<Paper> Paper { get; set; }
        public virtual ICollection<Competition> Competition { get; set; }
        public virtual ICollection<Project> Project { get; set; }
        public virtual ICollection<Research> Research { get; set; }
        public virtual ICollection<Fund> Fund { get; set; }
        public virtual ICollection<Patent> Patent { get; set; }
        public virtual ICollection<Serve> Serve { get; set; }
        public virtual ICollection<Construct> Construct { get; set; }
        public virtual ICollection<SafeAndProduce> SafeAndProduce { get; set; }
        public virtual ICollection<Maintenance> Maintenance { get; set; }
        public virtual ICollection<Charge> Charge { get; set; }
        public virtual ICollection<OtherContribute> OtherContribute { get; set; }

    }


}